<?php

namespace App\Http\Controllers;

use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

//For external users, add the following in env file: API_BASE_URL=https://calm-reef-68801.herokuapp.com/api/
class MoviesController extends Controller
{

    public function index($page = 1)
    {
        $movies = $this->request->get( $this->baseUrl . 'movies?page='.$page)
        ->json();

        if(empty($movies["data"])){
            abort(204);
        }

        return view('movies',[
            'movies' => $movies
        ]);
    }

    public function show($movie = null)
    {
        $movie = $this->request->get( $this->baseUrl . 'movies/'.$movie)
                        ->json()['data'];

        return view('show', compact("movie"));
    }
}
