<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{


    public function index()
    {
        $popular = $this->request->get( $this->baseUrl . 'movies/?per_page=10&sortByDesc=likes')
        ->json()['data'];

        $latest = $this->request->get( $this->baseUrl . 'movies/?per_page=5&sortByDesc=created_at')
                ->json()['data'];


        return view('index',[
            'popular' => $popular,
            'latest'  => $latest
        ]);
    }
}
